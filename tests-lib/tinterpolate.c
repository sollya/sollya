#include <sollya.h>

int no_msg(sollya_msg_t msg, void *data) {
  (void)msg;
  (void)data;
  return 0;
}

static const char* inputsX[] = { "[| 1, 2, 3 |]",
				 "[| 3, 4, 5, 6, 7, 8, 9 |]",
				 "[| 1 |]",
				 "[| 1, 2 |]",
				 "[| -2, -1, 1, 2 |]",
				 "[| 0, 1, 3, 7, 9, 11, 17, 25 |]",
				 NULL };

static const char* inputsY[] = { "[| 3, 5, 8 |]",
				 "[| 17, 42, 17, 42, 17, 42, 17 |]",
				 "[| 1664 |]",
				 "[| 1, 2 |]",
				 "exp(_x_)",
				 "1/17 + _x_ * (2/23 + _x_ * (3/17 + _x_ * 4/23))",
				 "sin(_x_)",
				 NULL };

static const char* inputsDelta[] = { "1/2",
				     "1b-2000",
				     "1b-53",
				     NULL };

static const char* inputsInterval[] = { "[ 1, 3 ]",
					"[ 3, 9 ]",
					"[ 0, 25 ]",
					"[-1b100;1b100]",
					NULL };
int main(void) {
  sollya_obj_t x, y, delta, interval, p;
  int i, j, k, l;

  sollya_lib_init();
  sollya_lib_install_msg_callback(no_msg, NULL);

  for (i=0; inputsX[i] != NULL; i++) {
    x = sollya_lib_parse_string(inputsX[i]);
    for (j=0; inputsY[j] != NULL; j++) {
      y = sollya_lib_parse_string(inputsY[j]);
      p = sollya_lib_interpolate(x, y, NULL);
      sollya_lib_printf("interpolate(%b, %b) = %b\n", x, y, p);
      sollya_lib_clear_obj(p);
      for (k=0; inputsDelta[k] != NULL; k++) {
	delta = sollya_lib_parse_string(inputsDelta[k]);
	p = sollya_lib_interpolate(x, y, delta, NULL);
	sollya_lib_printf("interpolate(%b, %b, %b) = %b\n", x, y, delta, p);
	sollya_lib_clear_obj(p);
	for (l=0; inputsInterval[l] != NULL; l++) {
	  interval = sollya_lib_parse_string(inputsInterval[l]);
	  p = sollya_lib_interpolate(x, y, delta, interval, NULL);
	  sollya_lib_printf("interpolate(%b, %b, %b, %b) = %b\n", x, y, delta, interval, p);
	  sollya_lib_clear_obj(p);
	  sollya_lib_clear_obj(interval);	
	}
	sollya_lib_clear_obj(delta);	
      }
      sollya_lib_clear_obj(y);
    }
    sollya_lib_clear_obj(x);
  }
  
  sollya_lib_close();
  return 0;
}

