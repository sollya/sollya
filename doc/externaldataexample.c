#include <sollya.h>

int magic = 17;

static int __printMagicDoit(sollya_obj_t obj) {
  void *data;
  int *magic;
  
  if (sollya_lib_decompose_externaldata(&data, NULL, obj)) {
    magic = (int *) data;
    printf("The magic is %d\n", *magic);
    *magic = *magic + 1;
  }

  return 1;
}

int printMagic(void **args) {
  return __printMagicDoit((sollya_obj_t) (args[0]));
}

