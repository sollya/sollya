<a name="externaldata"></a> 
<div class="divName"> 
<h2 class="name">Name:</h2> <?php linkTo("command","externaldata","externaldata");?> 
<span class="smallDescription">binds an external data symbol to a Sollya identifier 
</span> 
</div> 
<div class="divLibraryName"> 
<h2 class="libraryname">Library name:</h2> 
<span class="commandline type">&nbsp;&nbsp;&nbsp;&nbsp;sollya_obj_t sollya_lib_externaldata(char *, void *, void (*)(void *));</span> 
</div> 
<div class="divUsage"> 
<h2 class="category">Usage: </h2> 
<span class="commandline"><?php linkTo("command","externaldata","externaldata");?>(<span class="arg">identifier</span>, <span class="arg">filename</span>) : (<span class="type">identifier type</span>, <span class="type">string</span>) -&gt; <span class="type">void</span></span> 
 
</div> 
<div class="divParameters"> 
<h2 class="category">Parameters: </h2> 
<ul> 
<li><span class="arg">identifier</span> represents the identifier the external symbol is to be bound to</li> 
<li><span class="arg">filename</span> of type <span class="type">string</span> represents the name of the object file where the external symbol can be found</li> 
</ul> 
</div> 
<div class="divDescription"> 
<h2 class="category">Description: </h2><ul> 
<li><?php linkTo("command","externaldata","externaldata");?> allows for binding the Sollya identifier <span class="arg">identifier</span> to an 
external symbol, looked up in a dynamically linked library. After this 
binding, Sollya can retrieve the bound object in its own symbol 
table, can copy it, can assign it to other variables, pass is as 
arguments to procedures and externalprocedures, compare it 
etc. However, Sollya stays agnostic of the symbol's meaning and 
type. Due to this reason, the use of <?php linkTo("command","externaldata","externaldata");?> makes no sense but in 
conjuction with the use of <?php linkTo("command","externalproc","externalproc");?>, where an external procedure 
is bound to Sollya. This external procedure may know the type and 
meaning of the object bound with <?php linkTo("command","externaldata","externaldata");?> and display it, handle it, 
execute it etc. The same functionality may also be used for tools that 
use Sollya thru its library interface to bind their own objects to 
Sollya, pass them thru Sollya code and retrieve them eventually. 
</ul> 
</div> 
<div class="divExamples"> 
<div class="divExample"> 
<h2 class="category">Example 1: </h2> 
&nbsp;&nbsp;&nbsp;&gt; bashexecute("gcc -fPIC -Wall -I.. -I. -c externaldataexample.c");<br> 
&nbsp;&nbsp;&nbsp;&gt; bashexecute("gcc -fPIC -shared -o externaldataexample externaldataexample.o");<br> 
&nbsp;&nbsp;&nbsp;&gt; externaldata(magic, "./externaldataexample");<br> 
&nbsp;&nbsp;&nbsp;&gt; magic;<br> 
&nbsp;&nbsp;&nbsp;magic<br> 
&nbsp;&nbsp;&nbsp;&gt; print(magic);<br> 
&nbsp;&nbsp;&nbsp;magic<br> 
&nbsp;&nbsp;&nbsp;&gt; magic == magic;<br> 
&nbsp;&nbsp;&nbsp;true<br> 
&nbsp;&nbsp;&nbsp;&gt; magic == 17;<br> 
&nbsp;&nbsp;&nbsp;false<br> 
&nbsp;&nbsp;&nbsp;&gt; zauberei = magic;<br> 
&nbsp;&nbsp;&nbsp;&gt; zauberei;<br> 
&nbsp;&nbsp;&nbsp;magic<br> 
&nbsp;&nbsp;&nbsp;&gt; zauberei == magic;<br> 
&nbsp;&nbsp;&nbsp;true<br> 
</div> 
<div class="divExample"> 
<h2 class="category">Example 2: </h2> 
&nbsp;&nbsp;&nbsp;&gt; bashexecute("gcc -fPIC -Wall -I.. -I. -c externaldataexample.c");<br> 
&nbsp;&nbsp;&nbsp;&gt; bashexecute("gcc -fPIC -shared -o externaldataexample externaldataexample.o");<br> 
&nbsp;&nbsp;&nbsp;&gt; externaldata(magic, "./externaldataexample");<br> 
&nbsp;&nbsp;&nbsp;&gt; externalproc(printMagic, "./externaldataexample", object -&gt; void);<br> 
&nbsp;&nbsp;&nbsp;&gt; zauberei = magic;<br> 
&nbsp;&nbsp;&nbsp;&gt; printMagic;<br> 
&nbsp;&nbsp;&nbsp;printMagic<br> 
&nbsp;&nbsp;&nbsp;&gt; printMagic(magic);<br> 
&nbsp;&nbsp;&nbsp;The magic is 17<br> 
&nbsp;&nbsp;&nbsp;&gt; printMagic(magic);<br> 
&nbsp;&nbsp;&nbsp;The magic is 18<br> 
&nbsp;&nbsp;&nbsp;&gt; printMagic(zauberei);<br> 
&nbsp;&nbsp;&nbsp;The magic is 19<br> 
&nbsp;&nbsp;&nbsp;&gt; procedure magicAsParameter(m) {<br> 
&nbsp;&nbsp;&nbsp;&nbsp; 	&nbsp;&nbsp;m;<br> 
&nbsp;&nbsp;&nbsp;&nbsp; 	&nbsp;&nbsp;printMagic(m);<br> 
&nbsp;&nbsp;&nbsp;&nbsp; 	&nbsp;&nbsp;m;<br> 
&nbsp;&nbsp;&nbsp;&nbsp; };<br> 
&nbsp;&nbsp;&nbsp;&gt; magicAsParameter(magic);<br> 
&nbsp;&nbsp;&nbsp;magic<br> 
&nbsp;&nbsp;&nbsp;The magic is 20<br> 
&nbsp;&nbsp;&nbsp;magic<br> 
&nbsp;&nbsp;&nbsp;&gt; magicAsParameter(zauberei);<br> 
&nbsp;&nbsp;&nbsp;magic<br> 
&nbsp;&nbsp;&nbsp;The magic is 21<br> 
&nbsp;&nbsp;&nbsp;magic<br> 
</div> 
</div> 
<div class="divSeeAlso"> 
<span class="category">See also: </span><?php linkTo("command","library","library");?>, <?php linkTo("command","libraryconstant","libraryconstant");?>, <?php linkTo("command","externalproc","externalproc");?>, <?php linkTo("command","externalplot","externalplot");?>, <?php linkTo("command","bashexecute","bashexecute");?>, <?php linkTo("command","void","void");?>, <?php linkTo("command","constant","constant");?>, <?php linkTo("command","function","function");?>, <?php linkTo("command","range","range");?>, <?php linkTo("command","integer","integer");?>, <?php linkTo("command","string","string");?>, <?php linkTo("command","boolean","boolean");?>, <?php linkTo("command","listof","list of");?>, <?php linkTo("command","object","object");?> 
</div> 
