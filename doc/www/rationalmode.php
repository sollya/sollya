<a name="rationalmode"></a> 
<div class="divName"> 
<h2 class="name">Name:</h2> <?php linkTo("command","rationalmode","rationalmode");?> 
<span class="smallDescription">global variable controlling if rational arithmetic is used or not. 
</span> 
</div> 
<div class="divLibraryName"> 
<h2 class="libraryname">Library names:</h2> 
<span class="commandline type">void sollya_lib_set_rationalmode_and_print(sollya_obj_t)</span> 
<span class="commandline type">void sollya_lib_set_rationalmode(sollya_obj_t)</span> 
<span class="commandline type">sollya_obj_t sollya_lib_get_rationalmode()</span> 
</div> 
<div class="divUsage"> 
<h2 class="category">Usage: </h2> 
<span class="commandline"><?php linkTo("command","rationalmode","rationalmode");?> = <span class="arg">activation value</span> : <span class="type">on|off</span> -&gt; <span class="type">void</span></span> 
<span class="commandline"><?php linkTo("command","rationalmode","rationalmode");?> = <span class="arg">activation value</span> ! : <span class="type">on|off</span> -&gt; <span class="type">void</span></span> 
<span class="commandline"><?php linkTo("command","rationalmode","rationalmode");?> : <span class="type">on|off</span></span> 
 
</div> 
<div class="divParameters"> 
<h2 class="category">Parameters: </h2> 
<ul> 
<li><span class="arg">activation value</span> controls if rational arithmetic should be used or not</li> 
</ul> 
</div> 
<div class="divDescription"> 
<h2 class="category">Description: </h2><ul> 
<li><?php linkTo("command","rationalmode","rationalmode");?> is a global variable. When its value is <?php linkTo("command","off","off");?>, which is the 
default, Sollya will not use rational arithmetic to simplify expressions. All 
computations, including the evaluation of constant expressions given on the 
Sollya prompt, will be performed using floating-point and interval 
arithmetic. 
Constant expressions will be approximated by floating-point numbers, which 
are in most cases faithful roundings of the expressions, when shown at the 
prompt. 
</li><li>When the value of the global variable <?php linkTo("command","rationalmode","rationalmode");?> is <?php linkTo("command","on","on");?>, Sollya will use 
rational arithmetic when simplifying expressions. Constant expressions, given 
at the Sollya prompt, will be simplified to rational numbers and displayed 
as such when they are in the set of the rational numbers. Otherwise, 
floating-point and interval arithmetic will be used to compute a 
floating-point approximation, which is in most cases a faithful rounding of 
the constant expression. 
</li><li>When a decimal value is parsed, the behavior of Sollya is different 
depending on the value of the global variable <?php linkTo("command","rationalmode","rationalmode");?>. If it is <?php linkTo("command","off","off");?>, the 
value gets rounded as a floating-point at precision <?php linkTo("command","prec","prec");?>. But if <?php linkTo("command","rationalmode","rationalmode");?> 
is set to <?php linkTo("command","on","on");?>, the decimal value is interpreted exactly and converted as a 
rational number of the form M/10^N where M and N are integers. Accordingly, 
when <?php linkTo("command","rationalmode","rationalmode");?> is set to <?php linkTo("command","on","on");?> and <?php linkTo("command","display","display");?> is set to <?php linkTo("command","decimal","decimal");?>, all 
floating-point values are displayed exactly: indeed, any floating-point 
number with radix 2 has a finite decimal expansion. Therefore, any rational 
number of the form M/(2^P*5^Q) gets displayed as an exact decimal value 
(while other fractions get displayed as fractions). 
</ul> 
</div> 
<div class="divExamples"> 
<div class="divExample"> 
<h2 class="category">Example 1: </h2> 
&nbsp;&nbsp;&nbsp;&gt; rationalmode=off!;<br> 
&nbsp;&nbsp;&nbsp;&gt; 19/17 + 3/94;<br> 
&nbsp;&nbsp;&nbsp;1.1495619524405506883604505632040050062578222778473<br> 
&nbsp;&nbsp;&nbsp;&gt; rationalmode=on!;<br> 
&nbsp;&nbsp;&nbsp;&gt; 19/17 + 3/94;<br> 
&nbsp;&nbsp;&nbsp;1837 / 1598<br> 
</div> 
<div class="divExample"> 
<h2 class="category">Example 2: </h2> 
&nbsp;&nbsp;&nbsp;&gt; rationalmode=off!;<br> 
&nbsp;&nbsp;&nbsp;&gt; exp(19/17 + 3/94);<br> 
&nbsp;&nbsp;&nbsp;3.1568097739551413675470920894482427634032816281442<br> 
&nbsp;&nbsp;&nbsp;&gt; rationalmode=on!;<br> 
&nbsp;&nbsp;&nbsp;&gt; exp(19/17 + 3/94);<br> 
&nbsp;&nbsp;&nbsp;3.1568097739551413675470920894482427634032816281441796574919218482279430946893345552216426846046106958476007343642301414070837350944742638603202067315578460693359375<br> 
</div> 
<div class="divExample"> 
<h2 class="category">Example 3: </h2> 
&nbsp;&nbsp;&nbsp;&gt; prec = 12!;<br> 
&nbsp;&nbsp;&nbsp;&gt; rationalmode=off!;<br> 
&nbsp;&nbsp;&nbsp;&gt; r = 0.1;<br> 
&nbsp;&nbsp;&nbsp;&gt; r == 1/10;<br> 
&nbsp;&nbsp;&nbsp;false<br> 
&nbsp;&nbsp;&nbsp;&gt; rationalmode=on!;<br> 
&nbsp;&nbsp;&nbsp;&gt; s = 0.1;<br> 
&nbsp;&nbsp;&nbsp;&gt; s == 1/10;<br> 
&nbsp;&nbsp;&nbsp;true<br> 
&nbsp;&nbsp;&nbsp;&gt; r == s;<br> 
&nbsp;&nbsp;&nbsp;false<br> 
&nbsp;&nbsp;&nbsp;&gt; r;<br> 
&nbsp;&nbsp;&nbsp;0.100006103515625<br> 
&nbsp;&nbsp;&nbsp;&gt; s;<br> 
&nbsp;&nbsp;&nbsp;0.1<br> 
</div> 
</div> 
<div class="divSeeAlso"> 
<span class="category">See also: </span><?php linkTo("command","on","on");?>, <?php linkTo("command","off","off");?>, <?php linkTo("command","numerator","numerator");?>, <?php linkTo("command","denominator","denominator");?>, <?php linkTo("command","simplify","simplify");?>, <?php linkTo("command","rationalapprox","rationalapprox");?>, <?php linkTo("command","autosimplify","autosimplify");?> 
</div> 
