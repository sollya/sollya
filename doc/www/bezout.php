<a name="bezout"></a> 
<div class="divName"> 
<h2 class="name">Name:</h2> <?php linkTo("command","bezout","bezout");?> 
<span class="smallDescription">Computes the gcd as well as the co-factors (Bezout coefficients) 
</span> 
</div> 
<div class="divLibraryName"> 
<h2 class="libraryname">Library name:</h2> 
<span class="commandline type">sollya_obj_t sollya_lib_bezout(sollya_obj_t, sollya_obj_t)</span> 
</div> 
<div class="divUsage"> 
<h2 class="category">Usage: </h2> 
<span class="commandline"><?php linkTo("command","bezout","bezout");?>(<span class="arg">p</span>, <span class="arg">q</span>) : (<span class="type">function</span>, <span class="type">function</span>) -&gt; <span class="type">structure</span></span> 
 
</div> 
<div class="divParameters"> 
<h2 class="category">Parameters: </h2> 
<ul> 
<li><span class="arg">p</span> is a constant or a polynomial.</li> 
<li><span class="arg">q</span> is a constant or a polynomial.</li> 
</ul> 
</div> 
<div class="divDescription"> 
<h2 class="category">Description: </h2><ul> 
<li>When both <span class="arg">p</span> and <span class="arg">q</span> are integers, <?php linkTo("command","bezout","bezout");?>(<span class="arg">p</span>,<span class="arg">q</span>) computes the greatest 
common divisor g of these two integers, i.e., the greatest non-negative 
integer g dividing both <span class="arg">p</span> and <span class="arg">q</span>, as well as co-factors (Bezout 
coefficients), i.e., a and b such that a * p + b * q = g. 
</li><li><?php linkTo("command","bezout","bezout");?> always returns a structure containing four elements, g, r, a and b. 
These four elements always satisfy the following two properties: 
gcd(g,r) = gcd(p,q) and a * p + b * q = g. In addition, the tool tries to 
ensure r = 0. This latter property cannot always be ensured, in particular, 
it cannot be ensured for constant expression such that r becomes zero but 
the tool is unable to prove this fact. <?php linkTo("command","bezout","bezout");?> ensures that g is the greatest 
common divisor of <span class="arg">p</span> and <span class="arg">q</span> only if r ends up being zero. 
</li><li>When both <span class="arg">p</span> and <span class="arg">q</span> are rational numbers, say s/t and u/v, 
<?php linkTo("command","bezout","bezout");?>(<span class="arg">p</span>,<span class="arg">q</span>) computes the greatest common divisor of s * v and u * t, 
divided by the product of the denominators, t * v, as well as co-factors, 
still satisfying a * p + b * q = g. 
</li><li>When both <span class="arg">p</span> and <span class="arg">q</span> are constants but at least one of them is no rational 
number, <?php linkTo("command","bezout","bezout");?> does not run the extended Euclidian algorithm to reduce 
gcd(p,q), i.e., the two elements g, r of the returned structure contain 
copies of the inputs <span class="arg">p</span> and <span class="arg">q</span>. The elements a and b are set to 0 and 1 
accordingly. <?php linkTo("command","bezout","bezout");?> differs from the <?php linkTo("command","gcd","gcd");?> command in this point. 
</li><li>When both <span class="arg">p</span> and <span class="arg">q</span> are polynomials with at least one being non-constant, 
<?php linkTo("command","bezout","bezout");?>(<span class="arg">p</span>,<span class="arg">q</span>) returns the polynomial of greatest degree dividing both <span class="arg">p</span> 
and <span class="arg">q</span>, and whose leading coefficient is the greatest common divisor of the 
leading coefficients of <span class="arg">p</span> and <span class="arg">q</span>. Polynomial co-factors (Bezout 
coefficients), i.e., a and b such that a * p + b * q = g, are returned as 
well. 
</li><li>Similarly to the cases documented for <?php linkTo("command","diveucl","div");?> and <?php linkTo("command","modeucl","mod");?>, <?php linkTo("command","bezout","bezout");?> may fail to 
return the unique polynomial of largest degree dividing both <span class="arg">p</span> and <span class="arg">q</span> in 
cases when certain coefficients of either <span class="arg">p</span> or <span class="arg">q</span> are constant expressions 
for which the tool is unable to determine whether they are zero or not. These 
cases typically involve polynomials whose leading coefficient is zero but the 
tool is unable to detect this fact. In contrast to the <?php linkTo("command","gcd","gcd");?> command, the 
<?php linkTo("command","bezout","bezout");?> command is completely safe in this respect, as it returns not only 
an alleged greatest common divisor g but also the final remainder r obtained 
in the extended Euclidian algorithm. The two properties gcd(g,r) = gcd(p,q) 
and a * p + b * q = g are always satisfied for the four elements of the 
returned structure. 
</li><li>When at least one of <span class="arg">p</span> or <span class="arg">q</span> is a function that is no polynomial, <?php linkTo("command","bezout","bezout");?> 
does not run the extended Euclidian algorithm to reduce gcd(p,q), i.e., the 
two elements g, r of the returned structure contain copies of the inputs <span class="arg">p</span> 
and <span class="arg">q</span>. The elements a and b are set to 0 and 1 accordingly. <?php linkTo("command","bezout","bezout");?> differs 
from the <?php linkTo("command","gcd","gcd");?> command in this point. 
</ul> 
</div> 
<div class="divExamples"> 
<div class="divExample"> 
<h2 class="category">Example 1: </h2> 
&nbsp;&nbsp;&nbsp;&gt; bezout(1001, 231);<br> 
&nbsp;&nbsp;&nbsp;{ .b = -4, .a = 1, .r = 0, .g = 77 }<br> 
&nbsp;&nbsp;&nbsp;&gt; bezout(13, 17);<br> 
&nbsp;&nbsp;&nbsp;{ .b = -3, .a = 4, .r = 0, .g = 1 }<br> 
&nbsp;&nbsp;&nbsp;&gt; bezout(-210, 462);<br> 
&nbsp;&nbsp;&nbsp;{ .b = 1, .a = 2, .r = 0, .g = 42 }<br> 
</div> 
<div class="divExample"> 
<h2 class="category">Example 2: </h2> 
&nbsp;&nbsp;&nbsp;&gt; rationalmode = on!;<br> 
&nbsp;&nbsp;&nbsp;&gt; bezout(6/7, 33/13);<br> 
&nbsp;&nbsp;&nbsp;{ .b = -1, .a = 3, .r = 0, .g = 3 / 91 }<br> 
</div> 
<div class="divExample"> 
<h2 class="category">Example 3: </h2> 
&nbsp;&nbsp;&nbsp;&gt; bezout(exp(13),sin(17));<br> 
&nbsp;&nbsp;&nbsp;{ .b = 0, .a = 1, .r = -0.96139749187955685726163694486915609849206725405894, .g = 4.4241339200892050332610277594908828178439130606059e5 }<br> 
</div> 
<div class="divExample"> 
<h2 class="category">Example 4: </h2> 
&nbsp;&nbsp;&nbsp;&gt; bezout(24 + 68 * x + 74 * x^2 + 39 * x^3 + 10 * x^4 + x^5, 480 + 776 * x + 476 * x^2 + 138 * x^3 + 19 * x^4 + x^5);<br> 
&nbsp;&nbsp;&nbsp;{ .b = 9.1666666666666666666666666666666666666666666666665e-2 + x * (0.125 + x * 5e-2), .a = -1.6666666666666666666666666666666666666666666666667 + x * (-0.575 + x * (-5e-2)), .r = 0, .g = 4 + x * (4 + x) }<br> 
&nbsp;&nbsp;&nbsp;&gt; bezout(1001 * x^2, 231 * x);<br> 
&nbsp;&nbsp;&nbsp;{ .b = 0.33333333333333333333333333333333333333333333333333, .a = 0, .r = 0, .g = x * 77 }<br> 
</div> 
<div class="divExample"> 
<h2 class="category">Example 5: </h2> 
&nbsp;&nbsp;&nbsp;&gt; bezout(exp(x), x^2);<br> 
&nbsp;&nbsp;&nbsp;{ .b = 0, .a = 1, .r = x^2, .g = exp(x) }<br> 
</div> 
</div> 
<div class="divSeeAlso"> 
<span class="category">See also: </span><?php linkTo("command","gcd","gcd");?>, <?php linkTo("command","diveucl","div");?>, <?php linkTo("command","modeucl","mod");?>, <?php linkTo("command","numberroots","numberroots");?> 
</div> 
