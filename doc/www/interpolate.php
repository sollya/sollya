<a name="interpolate"></a> 
<div class="divName"> 
<h2 class="name">Name:</h2> <?php linkTo("command","interpolate","interpolate");?> 
<span class="smallDescription">computes an interpolation polynomial. 
</span> 
</div> 
<div class="divLibraryName"> 
<h2 class="libraryname">Library names:</h2> 
<span class="commandline type">sollya_obj_t sollya_lib_interpolate(sollya_obj_t, sollya_obj_t, ...)</span> 
<span class="commandline type">sollya_obj_t sollya_lib_v_interpolate(sollya_obj_t, sollya_obj_t, va_list)</span> 
</div> 
<div class="divUsage"> 
<h2 class="category">Usage: </h2> 
<span class="commandline"><?php linkTo("command","interpolate","interpolate");?>(<span class="arg">X</span>, <span class="arg">Y</span>) : (<span class="type">list</span>, <span class="type">list</span>) -&gt; <span class="type">function</span></span> 
<span class="commandline"><?php linkTo("command","interpolate","interpolate");?>(<span class="arg">X</span>, <span class="arg">Y</span>, <span class="arg">D</span>) : (<span class="type">list</span>, <span class="type">list</span>, <span class="type">constant</span>) -&gt; <span class="type">function</span></span> 
<span class="commandline"><?php linkTo("command","interpolate","interpolate");?>(<span class="arg">X</span>, <span class="arg">Y</span>, <span class="arg">D</span>, <span class="arg">I</span>) : (<span class="type">list</span>, <span class="type">list</span>, <span class="type">constant</span>, <span class="type">range</span>) -&gt; <span class="type">function</span></span> 
<span class="commandline"><?php linkTo("command","interpolate","interpolate");?>(<span class="arg">X</span>, <span class="arg">f</span>) : (<span class="type">list</span>, <span class="type">function</span>) -&gt; <span class="type">function</span></span> 
<span class="commandline"><?php linkTo("command","interpolate","interpolate");?>(<span class="arg">X</span>, <span class="arg">f</span>, <span class="arg">D</span>) : (<span class="type">list</span>, <span class="type">function</span>, <span class="type">constant</span>) -&gt; <span class="type">function</span></span> 
<span class="commandline"><?php linkTo("command","interpolate","interpolate");?>(<span class="arg">X</span>, <span class="arg">f</span>, <span class="arg">D</span>, <span class="arg">I</span>) : (<span class="type">list</span>, <span class="type">function</span>, <span class="type">constant</span>, <span class="type">range</span>) -&gt; <span class="type">function</span></span> 
 
</div> 
<div class="divParameters"> 
<h2 class="category">Parameters: </h2> 
<ul> 
<li><span class="arg">X</span> is a list of constants for the interpolation abscissas</li> 
<li><span class="arg">Y</span> is a list of constants for the interpolation ordinates</li> 
<li><span class="arg">f</span> is a function to be interpolated</li> 
<li><span class="arg">D</span> is a constant defining the maximum permissible error</li> 
<li><span class="arg">I</span> is an interval defining the domain to be considered for the error</li> 
</ul> 
</div> 
<div class="divDescription"> 
<h2 class="category">Description: </h2><ul> 
<li><?php linkTo("command","interpolate","interpolate");?> computes an interpolation polynomial p of minimal degree such 
that for all given abscissa points xi in the list <span class="arg">X</span> the interpolation 
condition is fullfilled with p(xi) = yi, where yi is the corresponding entry 
in the list of ordinates <span class="arg">Y</span>. All entries of the lists <span class="arg">X</span> and <span class="arg">Y</span> need to be 
constant expressions; the entries in the list of abscissas <span class="arg">X</span> need to be 
pairwise distinct. If no parameter <span class="arg">D</span> is given, the interpolation is exact, 
i.e., an interpolation polynomial (expression) is formed such that the 
interpolation condition is satisfied without error. 
</li><li><?php linkTo("command","interpolate","interpolate");?> accepts a function <span class="arg">f</span> instead of a list of ordinates. When 
called in this manner, <?php linkTo("command","interpolate","interpolate");?> computes a polynomial p such that for all 
given abscissa points xi in the list <span class="arg">X</span> the interpolation condition is 
fullfilled with p(xi) = f(xi). All entries in the list <span class="arg">X</span> still need to be 
constant expressions and they must be pairwise distinct. If no parameter <span class="arg">D</span> 
is given, the interpolation stays exact, even if this means forming constant 
expressions for the ordinates f(xi). 
</li><li>As exact interpolation is often not needed and in order to speed up 
computations, <?php linkTo("command","interpolate","interpolate");?> accepts an optional parameter <span class="arg">D</span>, a positive 
constant, that defines the maximum permissible error. In this case, instead 
of computing the exact interpolation polynomial p as it is defined above, 
<?php linkTo("command","interpolate","interpolate");?> computes and returns an approximate interpolation polynomial p~ 
that does not differ from p more than the amount <span class="arg">D</span> with supremum norm over 
the interval I that is given by the interval of all abscissa points in <span class="arg">X</span>. 
<br> 
Formally, let I be the convex hull of all abscissa points in <span class="arg">X</span>; typically, 
the lower bound of I is the minimum value amongst all <span class="arg">X</span>, the upper bound is 
the maximum value amongst all <span class="arg">X</span>. Let p be the unique polynomial such that 
p(xi) = yi for all xi in the list <span class="arg">X</span> and yi either the corresponding entry 
in the list <span class="arg">Y</span> or yi = f(xi). Then <?php linkTo("command","interpolate","interpolate");?> returns p~ such that 
                           ||p~ - p ||^I &lt;= D. 
  
The support for <span class="arg">D</span> works for any list of distinct abscissa points <span class="arg">X</span> and 
for any list of ordinate points <span class="arg">Y</span> or function <span class="arg">f</span>. The algorithm is however 
optimized for the case when <span class="arg">X</span> is a list of floating-point (or rational) 
constants and a function <span class="arg">f</span>. When <span class="arg">f</span> is a polynomial of degree n and <span class="arg">X</span> 
contains at most n+1 abscissa points, <?php linkTo("command","interpolate","interpolate");?> may return <span class="arg">f</span> as-is, even 
though <span class="arg">D</span> has been provided; typically without performing any rounding on 
the coefficients of the polynomial <span class="arg">f</span>. 
</li><li>When a permissible-error argument <span class="arg">D</span> is given to <?php linkTo("command","interpolate","interpolate");?>, the command 
additionnally supports an optional interval parameter <span class="arg">I</span>. If this parameter 
is specified, the interval I to consider the error between p~ and p on is not 
taken as the convex hull of the abscissa points <span class="arg">X</span> but assumed to be the 
interval I. This parameter is supported to both allow for interpolation over 
intervals where the first (or last) interpolation point does not lie on the 
lower (resp. upper) bound of the interval and to speed up computation: 
determining the minimum and maximum values in <span class="arg">X</span> may be expensive. 
</li><li><?php linkTo("command","interpolate","interpolate");?> is completely insensible to the tool's working precision <?php linkTo("command","prec","prec");?>. 
It adapts its own working precision entirely automatically. However the user 
should be aware that precision adaption comes at a price: if the 
interpolation problem requires high precision to be solved, the execution 
time of <?php linkTo("command","interpolate","interpolate");?> will be high. This can typically be observed when the 
list of abscissas <span class="arg">X</span> contains two values that are mathematically different 
but for which high precision is needed to distinguish them. 
</li><li>In case of error, e.g., when the abscissa points in <span class="arg">X</span> are not all 
distinct, when the function does not evaluate at one of the abscissa points 
or when the permissible error <span class="arg">D</span> or domain interval <span class="arg">I</span> are not acceptable, 
<?php linkTo("command","interpolate","interpolate");?> evaluates to <?php linkTo("command","error","error");?>. 
</ul> 
</div> 
<div class="divExamples"> 
<div class="divExample"> 
<h2 class="category">Example 1: </h2> 
&nbsp;&nbsp;&nbsp;&gt; p = interpolate([| 1, 2, 3 |], [| 3, 5, 9 |]);<br> 
&nbsp;&nbsp;&nbsp;&gt; write("p = ", p, "\n");<br> 
&nbsp;&nbsp;&nbsp;p = 3 + _x_ * (-1 + _x_)<br> 
</div> 
<div class="divExample"> 
<h2 class="category">Example 2: </h2> 
&nbsp;&nbsp;&nbsp;&gt; p = interpolate([| 1, 2, 3, 4 |], [| 1, -1, 2, -3 |]);<br> 
&nbsp;&nbsp;&nbsp;&gt; write("p = ", p, "\n");<br> 
&nbsp;&nbsp;&nbsp;p = 21 + _x_ * (-100 / 3 + _x_ * (15.5 + _x_ * -13 / 6))<br> 
&nbsp;&nbsp;&nbsp;&gt; p = interpolate([| 1, 2, 3, 4 |], [| 1, -1, 2, -3 |], 2^-17);<br> 
&nbsp;&nbsp;&nbsp;&gt; write("p = ", p, "\n");<br> 
&nbsp;&nbsp;&nbsp;p = 21 + _x_ * (-33.333333492279052734375 + _x_ * (15.5 + _x_ * (-2.1666666567325592041015625)))<br> 
&nbsp;&nbsp;&nbsp;&gt; p = interpolate([| 1, 2, 3, 4 |], [| 1, -1, 2, -3 |], 2^-17, [0; 2^42]);<br> 
&nbsp;&nbsp;&nbsp;&gt; write("p = ", p, "\n");<br> 
&nbsp;&nbsp;&nbsp;p = 21 + _x_ * (-33.333333333333333333333333333333333333333333363228 + _x_ * (15.5 + _x_ * (-2.1666666666666666666666666666666666666666666647983)))<br> 
</div> 
<div class="divExample"> 
<h2 class="category">Example 3: </h2> 
&nbsp;&nbsp;&nbsp;&gt; p = interpolate([| 1, 2, 3, 4 |], [| 1, -1, 2, -3 |]);<br> 
&nbsp;&nbsp;&nbsp;&gt; q = interpolate([| 1, 2, 3, 4 |], [| 1, -1, 2, -3 |], 2^-17);<br> 
&nbsp;&nbsp;&nbsp;&gt; delta = horner(p - q);<br> 
&nbsp;&nbsp;&nbsp;&gt; write("delta = ", delta, "\n");<br> 
&nbsp;&nbsp;&nbsp;delta = _x_ * (1 / 6291456 + _x_^2 * -1 / 100663296)<br> 
&nbsp;&nbsp;&nbsp;&gt; Delta = dirtyinfnorm(delta,[1;4]);<br> 
&nbsp;&nbsp;&nbsp;&gt; "Delta = ", Delta, " = 2^(", log2(Delta), ")";<br> 
&nbsp;&nbsp;&nbsp;Delta = 2.4471294368728034316556666925301338011322095712879e-7 = 2^(-21.962406251802890453634347359869541271899536019231)<br> 
&nbsp;&nbsp;&nbsp;&gt; p = interpolate([| 1, 2, 3, 4 |], [| 1, -1, 2, -3 |]);<br> 
&nbsp;&nbsp;&nbsp;&gt; q = interpolate([| 1, 2, 3, 4 |], [| 1, -1, 2, -3 |], 2^-2000);<br> 
&nbsp;&nbsp;&nbsp;&gt; delta = horner(p - q);<br> 
&nbsp;&nbsp;&nbsp;&gt; write("delta = ", delta, "\n");<br> 
&nbsp;&nbsp;&nbsp;delta = _x_ * (1 / 4.4088218698531373730540794925222988186456999760206e604 + _x_^2 * -1 / 7.054114991765019796886527188035678109833119961633e605)<br> 
&nbsp;&nbsp;&nbsp;&gt; Delta = dirtyinfnorm(delta,[1;4]);<br> 
&nbsp;&nbsp;&nbsp;&gt; "Delta = ", Delta, " = 2^(", log2(Delta), ")";<br> 
&nbsp;&nbsp;&nbsp;Delta = 3.4920910013773992744476948245123751988401933577684e-605 = 2^(-2007.9624062518028904536343473598695412718995360192)<br> 
</div> 
<div class="divExample"> 
<h2 class="category">Example 4: </h2> 
&nbsp;&nbsp;&nbsp;&gt; p = interpolate([| 1, 2, 3, 4 |], [| 1, -1, 2, -3 |]);<br> 
&nbsp;&nbsp;&nbsp;&gt; q = interpolate([| 1, 2, 3, 4 |], [| 1, -1, 2, -3 |], 2^-17, [0;2^10]);<br> 
&nbsp;&nbsp;&nbsp;&gt; delta = horner(p - q);<br> 
&nbsp;&nbsp;&nbsp;&gt; write("delta = ", delta, "\n");<br> 
&nbsp;&nbsp;&nbsp;delta = _x_ * (1 / 422212465065984 + _x_^2 * -1 / 6755399441055744)<br> 
&nbsp;&nbsp;&nbsp;&gt; Delta = dirtyinfnorm(delta,[0;2^10]);<br> 
&nbsp;&nbsp;&nbsp;&gt; "Delta = ", Delta, " = 2^(", log2(Delta), ")";<br> 
&nbsp;&nbsp;&nbsp;Delta = 1.589432940818369388580322265625e-7 = 2^(-22.584984514668420136955755457026138868402791643092)<br> 
&nbsp;&nbsp;&nbsp;&gt; p = interpolate([| 1, 2, 3, 4 |], [| 1, -1, 2, -3 |]);<br> 
&nbsp;&nbsp;&nbsp;&gt; q = interpolate([| 1, 2, 3, 4 |], [| 1, -1, 2, -3 |], 2^-2000, [0;2^10]);<br> 
&nbsp;&nbsp;&nbsp;&gt; delta = horner(p - q);<br> 
&nbsp;&nbsp;&nbsp;&gt; write("delta = ", delta, "\n");<br> 
&nbsp;&nbsp;&nbsp;delta = _x_ * (-1 / 3.6983878408024986992700435663608496048481867984446e611 + _x_^2 * 1 / 5.9174205452839979188320697061773593677570988775114e612)<br> 
&nbsp;&nbsp;&nbsp;&gt; Delta = dirtyinfnorm(delta,[0;2^10]);<br> 
&nbsp;&nbsp;&nbsp;&gt; "Delta = ", Delta, " = 2^(", log2(Delta), ")";<br> 
&nbsp;&nbsp;&nbsp;Delta = 1.8145160239721784427448462541908690004638874174061e-604 = 2^(-2005.58498451466842013695575545702613886840279164307)<br> 
</div> 
<div class="divExample"> 
<h2 class="category">Example 5: </h2> 
&nbsp;&nbsp;&nbsp;&gt; p = interpolate([| 1, 2, 3, 4 |], 17 + _x_ * (42 + _x_ * (23 + _x_ * 1664)));<br> 
&nbsp;&nbsp;&nbsp;&gt; write("p = ", p, "\n");<br> 
&nbsp;&nbsp;&nbsp;p = 17 + _x_ * (42 + _x_ * (23 + _x_ * 1664))<br> 
</div> 
<div class="divExample"> 
<h2 class="category">Example 6: </h2> 
&nbsp;&nbsp;&nbsp;&gt; p = interpolate([| 1, 2, 3, 4 |], exp(_x_));<br> 
&nbsp;&nbsp;&nbsp;&gt; write("p = ", p, "\n");<br> 
&nbsp;&nbsp;&nbsp;p = -1 * (-2 * (-3 * ((exp(4) - exp(3) - (exp(3) - exp(2))) / 2 - (exp(3) - exp(2) - (exp(2) - exp(1))) / 2) / 3 + (exp(3) - exp(2) - (exp(2) - exp(1))) / 2) + exp(2) - exp(1)) + exp(1) + _x_ * (-1 * (-2 * ((exp(4) - exp(3) - (exp(3) - exp(2))) / 2 - (exp(3) - exp(2) - (exp(2) - exp(1))) / 2) / 3 + -3 * ((exp(4) - exp(3) - (exp(3) - exp(2))) / 2 - (exp(3) - exp(2) - (exp(2) - exp(1))) / 2) / 3 + (exp(3) - exp(2) - (exp(2) - exp(1))) / 2) + -2 * (-3 * ((exp(4) - exp(3) - (exp(3) - exp(2))) / 2 - (exp(3) - exp(2) - (exp(2) - exp(1))) / 2) / 3 + (exp(3) - exp(2) - (exp(2) - exp(1))) / 2) + exp(2) - exp(1) + _x_ * (-1 * ((exp(4) - exp(3) - (exp(3) - exp(2))) / 2 - (exp(3) - exp(2) - (exp(2) - exp(1))) / 2) / 3 + -2 * ((exp(4) - exp(3) - (exp(3) - exp(2))) / 2 - (exp(3) - exp(2) - (exp(2) - exp(1))) / 2) / 3 + -3 * ((exp(4) - exp(3) - (exp(3) - exp(2))) / 2 - (exp(3) - exp(2) - (exp(2) - exp(1))) / 2) / 3 + (exp(3) - exp(2) - (exp(2) - exp(1))) / 2 + _x_ * ((exp(4) - exp(3) - (exp(3) - exp(2))) / 2 - (exp(3) - exp(2) - (exp(2) - exp(1))) / 2) / 3))<br> 
&nbsp;&nbsp;&nbsp;&gt; p = interpolate([| 1, 2, 3, 4 |], exp(_x_), 2^-17);<br> 
&nbsp;&nbsp;&nbsp;&gt; write("p = ", p, "\n");<br> 
&nbsp;&nbsp;&nbsp;p = -7.7172116227447986602783203125 + _x_ * (17.9146616198122501373291015625 + _x_ * (-9.7775724567472934722900390625 + _x_ * 2.2984042889438569545745849609375))<br> 
&nbsp;&nbsp;&nbsp;&gt; p = interpolate([| 1, 2, 3, 4 |], exp(_x_), 2^-17, [0; 2^42]);<br> 
&nbsp;&nbsp;&nbsp;&gt; write("p = ", p, "\n");<br> 
&nbsp;&nbsp;&nbsp;p = -7.717211620141288536337557462573403702892010733189 + _x_ * (17.914661614969411928830253122773955194465923548481 + _x_ * (-9.7775724550214350407416860470959032694184512464826 + _x_ * 2.2984042886523568836092778582480142756017855238293))<br> 
</div> 
<div class="divExample"> 
<h2 class="category">Example 7: </h2> 
&nbsp;&nbsp;&nbsp;&gt; p = interpolate([| 1, 2, 3, 4 |], exp(_x_));<br> 
&nbsp;&nbsp;&nbsp;&gt; q = interpolate([| 1, 2, 3, 4 |], exp(_x_), 2^-17);<br> 
&nbsp;&nbsp;&nbsp;&gt; delta = horner(p - q);<br> 
&nbsp;&nbsp;&nbsp;&gt; Delta = dirtyinfnorm(delta,[1;4]);<br> 
&nbsp;&nbsp;&nbsp;&gt; "Delta = ", Delta, " = 2^(", log2(Delta), ")";<br> 
&nbsp;&nbsp;&nbsp;Delta = 7.8101123470606372346391215972092629613859312741733e-9 = 2^(-26.932009552561199842757802778211432954321538161928)<br> 
&nbsp;&nbsp;&nbsp;&gt; p = interpolate([| 1, 2, 3, 4 |], exp(_x_));<br> 
&nbsp;&nbsp;&nbsp;&gt; q = interpolate([| 1, 2, 3, 4 |], exp(_x_), 2^-2000);<br> 
&nbsp;&nbsp;&nbsp;&gt; delta = horner(p - q);<br> 
&nbsp;&nbsp;&nbsp;&gt; Delta = dirtyinfnorm(delta,[1;4]);<br> 
&nbsp;&nbsp;&nbsp;&gt; "Delta = ", Delta, " = 2^(", log2(Delta), ")";<br> 
&nbsp;&nbsp;&nbsp;Delta = 3.574737886097255322676984068596786349480238060537e-606 = 2^(-2011.2505880400718258175558034736121948862189809702)<br> 
</div> 
<div class="divExample"> 
<h2 class="category">Example 8: </h2> 
&nbsp;&nbsp;&nbsp;&gt; p = interpolate([| 1, 2, 3, 4 |], exp(_x_));<br> 
&nbsp;&nbsp;&nbsp;&gt; q = interpolate([| 1, 2, 3, 4 |], exp(_x_), 2^-17, [0;2^10]);<br> 
&nbsp;&nbsp;&nbsp;&gt; delta = horner(p - q);<br> 
&nbsp;&nbsp;&nbsp;&gt; Delta = dirtyinfnorm(delta,[0;2^10]);<br> 
&nbsp;&nbsp;&nbsp;&gt; "Delta = ", Delta, " = 2^(", log2(Delta), ")";<br> 
&nbsp;&nbsp;&nbsp;Delta = 3.3174992022654716926120201737930257514243356389991e-9 = 2^(-28.167256735304970710773265696018465191273351786528)<br> 
&nbsp;&nbsp;&nbsp;&gt; p = interpolate([| 1, 2, 3, 4 |], exp(_x_));<br> 
&nbsp;&nbsp;&nbsp;&gt; q = interpolate([| 1, 2, 3, 4 |], exp(_x_), 2^-2000, [0;2^10]);<br> 
&nbsp;&nbsp;&nbsp;&gt; delta = horner(p - q);<br> 
&nbsp;&nbsp;&nbsp;&gt; Delta = dirtyinfnorm(delta,[0;2^10]);<br> 
&nbsp;&nbsp;&nbsp;&gt; "Delta = ", Delta, " = 2^(", log2(Delta), ")";<br> 
&nbsp;&nbsp;&nbsp;Delta = 1.67480290063655159679957323422134460429582811153817e-605 = 2^(-2009.0225060850185477225876655278327362519640642038)<br> 
</div> 
<div class="divExample"> 
<h2 class="category">Example 9: </h2> 
&nbsp;&nbsp;&nbsp;&gt; f = exp(_x_);<br> 
&nbsp;&nbsp;&nbsp;&gt; I = [-1/2;1/2];<br> 
&nbsp;&nbsp;&nbsp;&gt; n = 17;<br> 
&nbsp;&nbsp;&nbsp;&gt; X = [||]; for i from 0 to n do X = (~(1/2 * (inf(I) + sup(I)) + 1/2 * (sup(I) - inf(I)) * cos(pi * ((2 * i + 1)/(2 * (n + 1)))))) .: X; X = revert(X);<br> 
&nbsp;&nbsp;&nbsp;&gt; p = interpolate(X, f, 2^-110);<br> 
&nbsp;&nbsp;&nbsp;&gt; q = remez(f, n, I);<br> 
&nbsp;&nbsp;&nbsp;&gt; Delta = dirtyinfnorm(p-f,I);<br> 
&nbsp;&nbsp;&nbsp;&gt; "||p-f|| = ", Delta, " = 2^(", log2(Delta), ")";<br> 
&nbsp;&nbsp;&nbsp;||p-f|| = 4.6822823648740795415790209572324990540532715520787e-27 = 2^(-87.464846623211506004130540250100341126958424233896)<br> 
&nbsp;&nbsp;&nbsp;&gt; Delta = dirtyinfnorm(q-f,I);<br> 
&nbsp;&nbsp;&nbsp;&gt; "||q-f|| = ", Delta, " = 2^(", log2(Delta), ")";<br> 
&nbsp;&nbsp;&nbsp;||q-f|| = 4.5615560104462964431174181483603567519672456911095e-27 = 2^(-87.502532530192383004694428978646385337892102159012)<br> 
&nbsp;&nbsp;&nbsp;&gt; Delta = dirtyinfnorm(horner(p-q),I);<br> 
&nbsp;&nbsp;&nbsp;&gt; "||p-q|| = ", Delta, " = 2^(", log2(Delta), ")";<br> 
&nbsp;&nbsp;&nbsp;||p-q|| = 1.20729201910945452306513389470528343007256343110758e-28 = 2^(-92.742211980284523389260465711218526032152621095553)<br> 
</div> 
<div class="divExample"> 
<h2 class="category">Example 10: </h2> 
&nbsp;&nbsp;&nbsp;&gt; interpolate([| 1, 1 + 2^-1000 |], 17 + 42 * x);<br> 
&nbsp;&nbsp;&nbsp;17 + x * 42<br> 
&nbsp;&nbsp;&nbsp;&gt; interpolate([| 1, 1 + 2^-10000 |], 17 + 42 * x);<br> 
&nbsp;&nbsp;&nbsp;17 + x * 42<br> 
&nbsp;&nbsp;&nbsp;&gt; interpolate([| 1, 1 |], 17 + 42 * x);<br> 
&nbsp;&nbsp;&nbsp;error<br> 
</div> 
</div> 
<div class="divSeeAlso"> 
<span class="category">See also: </span><?php linkTo("command","remez","remez");?>, <?php linkTo("command","dirtyinfnorm","dirtyinfnorm");?>, <?php linkTo("command","supnorm","supnorm");?>, <?php linkTo("command","prec","prec");?>, <?php linkTo("command","error","error");?> 
</div> 
