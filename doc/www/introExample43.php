<div class="divExample">
&nbsp;&nbsp;&nbsp;&gt; f = x - sin(x);<br>
&nbsp;&nbsp;&nbsp;&gt; [-1b-10;1b-10] - sin([-1b-10;1b-10]);<br>
&nbsp;&nbsp;&nbsp;Warning: For at least 2 of the constants displayed in decimal, rounding has happened.<br>
&nbsp;&nbsp;&nbsp;[-1.95312484477957829894e-3;1.95312484477957829894e-3]<br>
&nbsp;&nbsp;&nbsp;&gt; f([-1b-10;1b-10]);<br>
&nbsp;&nbsp;&nbsp;Warning: For at least 2 of the constants displayed in decimal, rounding has happened.<br>
&nbsp;&nbsp;&nbsp;[-1.552204217011176269e-10;1.552204217011176269e-10]<br>
&nbsp;&nbsp;&nbsp;&gt; evaluate(f,[-1b-10;1b-10]);<br>
&nbsp;&nbsp;&nbsp;Warning: For at least 2 of the constants displayed in decimal, rounding has happened.<br>
&nbsp;&nbsp;&nbsp;[-1.552204217011176269e-10;1.552204217011176269e-10]<br>
</div>
