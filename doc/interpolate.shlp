#NAME $INTERPOLATE
#QUICK_DESCRIPTION computes an interpolation polynomial.
#CALLING $COMMAND(<X>, <Y>)
#TYPE ($LIST_TYPE, $LIST_TYPE) -> $FUNCTION_TYPE

#CALLING $COMMAND(<X>, <Y>, <D>)
#TYPE ($LIST_TYPE, $LIST_TYPE, $CONSTANT_TYPE) -> $FUNCTION_TYPE

#CALLING $COMMAND(<X>, <Y>, <D>, <I>)
#TYPE ($LIST_TYPE, $LIST_TYPE, $CONSTANT_TYPE, $RANGE_TYPE) -> $FUNCTION_TYPE

#CALLING $COMMAND(<X>, <f>)
#TYPE ($LIST_TYPE, $FUNCTION_TYPE) -> $FUNCTION_TYPE

#CALLING $COMMAND(<X>, <f>, <D>)
#TYPE ($LIST_TYPE, $FUNCTION_TYPE, $CONSTANT_TYPE) -> $FUNCTION_TYPE

#CALLING $COMMAND(<X>, <f>, <D>, <I>)
#TYPE ($LIST_TYPE, $FUNCTION_TYPE, $CONSTANT_TYPE, $RANGE_TYPE) -> $FUNCTION_TYPE

#LIBRARYNAME sollya_obj_t sollya_lib_interpolate(sollya_obj_t, sollya_obj_t, ...)
#LIBRARYNAME sollya_obj_t sollya_lib_v_interpolate(sollya_obj_t, sollya_obj_t, va_list)

#PARAMETERS <X> is a list of constants for the interpolation abscissas
#PARAMETERS <Y> is a list of constants for the interpolation ordinates
#PARAMETERS <f> is a function to be interpolated
#PARAMETERS <D> is a constant defining the maximum permissible error
#PARAMETERS <I> is an interval defining the domain to be considered for the error

#DESCRIPTION
$COMMAND computes an interpolation polynomial §§p§$p$§§ of minimal degree such
that for all given abscissa points §§xi§$x_i$§§ in the list <X> the interpolation
condition is fullfilled with §§p(xi) = yi§$p(x_i) = y_i$§§, where §§yi§$y_i$§§ is the corresponding entry
in the list of ordinates <Y>. All entries of the lists <X> and <Y> need to be
constant expressions; the entries in the list of abscissas <X> need to be
pairwise distinct. If no parameter <D> is given, the interpolation is exact,
§§i.e.§\emph{i.e.}§§, an interpolation polynomial (expression) is formed such that the
interpolation condition is satisfied without error.

#DESCRIPTION
$COMMAND accepts a function <f> instead of a list of ordinates. When
called in this manner, $COMMAND computes a polynomial §§p§$p$§§ such that for all
given abscissa points §§xi§$x_i$§§ in the list <X> the interpolation condition is
fullfilled with §§p(xi) = f(xi)§$p(x_i) = f(x_i)$§§. All entries in the list <X> still need to be
constant expressions and they must be pairwise distinct. If no parameter <D>
is given, the interpolation stays exact, even if this means forming constant
expressions for the ordinates §§f(xi)§$f(x_i)$§§.

#DESCRIPTION
As exact interpolation is often not needed and in order to speed up
computations, $COMMAND accepts an optional parameter <D>, a positive
constant, that defines the maximum permissible error. In this case, instead
of computing the exact interpolation polynomial §§p§$p$§§ as it is defined above,
$COMMAND computes and returns an approximate interpolation polynomial §§p~§$\tilde{p}$§§
that does not differ from §§p§$p$§§ more than the amount <D> with supremum norm over
the interval §§I§$I$§§ that is given by the interval of all abscissa points in <X>.
§§ §\\§§
Formally, let §§I§$I$§§ be the convex hull of all abscissa points in <X>; typically,
the lower bound of §§I§$I$§§ is the minimum value amongst all <X>, the upper bound is
the maximum value amongst all <X>. Let §§p§$p$§§ be the unique polynomial such that
§§p(xi) = yi§$p(x_i) = y_i$§§ for all §§xi§$x_i$§§ in the list <X> and §§yi§$y_i$§§ either the corresponding entry
in the list <Y> or §§yi = f(xi)§$y_i = f(x_i)$§§. Then $COMMAND returns §§p~§$\tilde{p}$§§ such that
§§                           ||p~ - p ||^I <= D.§$$\| \tilde{p} - p \|_{\infty,I} \leq D.$$§§
§§ §§§
The support for <D> works for any list of distinct abscissa points <X> and
for any list of ordinate points <Y> or function <f>. The algorithm is however
optimized for the case when <X> is a list of floating-point (or rational)
constants and a function <f>. When <f> is a polynomial of degree §§n§$n$§§ and <X>
contains at most §§n+1§$n+1$§§ abscissa points, $COMMAND may return <f> as-is, even
though <D> has been provided; typically without performing any rounding on
the coefficients of the polynomial <f>.

#DESCRIPTION
When a permissible-error argument <D> is given to $COMMAND, the command
additionnally supports an optional interval parameter <I>. If this parameter
is specified, the interval §§I§$I$§§ to consider the error between §§p~§$\tilde{p}$§§ and §§p§$p$§§ on is not
taken as the convex hull of the abscissa points <X> but assumed to be the
interval §§I§$I$§§. This parameter is supported to both allow for interpolation over
intervals where the first (or last) interpolation point does not lie on the
lower (resp. upper) bound of the interval and to speed up computation:
determining the minimum and maximum values in <X> may be expensive.

#DESCRIPTION
$COMMAND is completely insensible to the tool's working precision $PREC.
It adapts its own working precision entirely automatically. However the user
should be aware that precision adaption comes at a price: if the
interpolation problem requires high precision to be solved, the execution
time of $COMMAND will be high. This can typically be observed when the
list of abscissas <X> contains two values that are mathematically different
but for which high precision is needed to distinguish them.

#DESCRIPTION
In case of error, §§e.g.§\emph{e.g.}§§, when the abscissa points in <X> are not all
distinct, when the function does not evaluate at one of the abscissa points
or when the permissible error <D> or domain interval <I> are not acceptable,
$COMMAND evaluates to $ERROR.


#EXAMPLE
p = interpolate([| 1, 2, 3 |], [| 3, 5, 9 |]);
write("p = ", p, "\n");

#EXAMPLE
p = interpolate([| 1, 2, 3, 4 |], [| 1, -1, 2, -3 |]);
write("p = ", p, "\n");
p = interpolate([| 1, 2, 3, 4 |], [| 1, -1, 2, -3 |], 2^-17);
write("p = ", p, "\n");
p = interpolate([| 1, 2, 3, 4 |], [| 1, -1, 2, -3 |], 2^-17, [0; 2^42]);
write("p = ", p, "\n");

#EXAMPLE
p = interpolate([| 1, 2, 3, 4 |], [| 1, -1, 2, -3 |]);
q = interpolate([| 1, 2, 3, 4 |], [| 1, -1, 2, -3 |], 2^-17);
delta = horner(p - q);
write("delta = ", delta, "\n");
Delta = dirtyinfnorm(delta,[1;4]);
"Delta = ", Delta, " = 2^(", log2(Delta), ")";
p = interpolate([| 1, 2, 3, 4 |], [| 1, -1, 2, -3 |]);
q = interpolate([| 1, 2, 3, 4 |], [| 1, -1, 2, -3 |], 2^-2000);
delta = horner(p - q);
write("delta = ", delta, "\n");
Delta = dirtyinfnorm(delta,[1;4]);
"Delta = ", Delta, " = 2^(", log2(Delta), ")";

#EXAMPLE
p = interpolate([| 1, 2, 3, 4 |], [| 1, -1, 2, -3 |]);
q = interpolate([| 1, 2, 3, 4 |], [| 1, -1, 2, -3 |], 2^-17, [0;2^10]);
delta = horner(p - q);
write("delta = ", delta, "\n");
Delta = dirtyinfnorm(delta,[0;2^10]);
"Delta = ", Delta, " = 2^(", log2(Delta), ")";
p = interpolate([| 1, 2, 3, 4 |], [| 1, -1, 2, -3 |]);
q = interpolate([| 1, 2, 3, 4 |], [| 1, -1, 2, -3 |], 2^-2000, [0;2^10]);
delta = horner(p - q);
write("delta = ", delta, "\n");
Delta = dirtyinfnorm(delta,[0;2^10]);
"Delta = ", Delta, " = 2^(", log2(Delta), ")";

#EXAMPLE
p = interpolate([| 1, 2, 3, 4 |], 17 + _x_ * (42 + _x_ * (23 + _x_ * 1664)));
write("p = ", p, "\n");

#EXAMPLE
p = interpolate([| 1, 2, 3, 4 |], exp(_x_));
write("p = ", p, "\n");
p = interpolate([| 1, 2, 3, 4 |], exp(_x_), 2^-17);
write("p = ", p, "\n");
p = interpolate([| 1, 2, 3, 4 |], exp(_x_), 2^-17, [0; 2^42]);
write("p = ", p, "\n");

#EXAMPLE
p = interpolate([| 1, 2, 3, 4 |], exp(_x_));
q = interpolate([| 1, 2, 3, 4 |], exp(_x_), 2^-17);
delta = horner(p - q);
Delta = dirtyinfnorm(delta,[1;4]);
"Delta = ", Delta, " = 2^(", log2(Delta), ")";
p = interpolate([| 1, 2, 3, 4 |], exp(_x_));
q = interpolate([| 1, 2, 3, 4 |], exp(_x_), 2^-2000);
delta = horner(p - q);
Delta = dirtyinfnorm(delta,[1;4]);
"Delta = ", Delta, " = 2^(", log2(Delta), ")";

#EXAMPLE
p = interpolate([| 1, 2, 3, 4 |], exp(_x_));
q = interpolate([| 1, 2, 3, 4 |], exp(_x_), 2^-17, [0;2^10]);
delta = horner(p - q);
Delta = dirtyinfnorm(delta,[0;2^10]);
"Delta = ", Delta, " = 2^(", log2(Delta), ")";
p = interpolate([| 1, 2, 3, 4 |], exp(_x_));
q = interpolate([| 1, 2, 3, 4 |], exp(_x_), 2^-2000, [0;2^10]);
delta = horner(p - q);
Delta = dirtyinfnorm(delta,[0;2^10]);
"Delta = ", Delta, " = 2^(", log2(Delta), ")";

#EXAMPLE
f = exp(_x_);
I = [-1/2;1/2];
n = 17;
X = [||]; for i from 0 to n do X = (~(1/2 * (inf(I) + sup(I)) + 1/2 * (sup(I) - inf(I)) * cos(pi * ((2 * i + 1)/(2 * (n + 1)))))) .: X; X = revert(X);
p = interpolate(X, f, 2^-110);
q = remez(f, n, I);
Delta = dirtyinfnorm(p-f,I);
"||p-f|| = ", Delta, " = 2^(", log2(Delta), ")";
Delta = dirtyinfnorm(q-f,I);
"||q-f|| = ", Delta, " = 2^(", log2(Delta), ")";
Delta = dirtyinfnorm(horner(p-q),I);
"||p-q|| = ", Delta, " = 2^(", log2(Delta), ")";

#EXAMPLE
interpolate([| 1, 1 + 2^-1000 |], 17 + 42 * x);
interpolate([| 1, 1 + 2^-10000 |], 17 + 42 * x);
interpolate([| 1, 1 |], 17 + 42 * x);

#SEEALSO $REMEZ
#SEEALSO $DIRTYINFNORM
#SEEALSO $SUPNORM
#SEEALSO $PREC
#SEEALSO $ERROR
