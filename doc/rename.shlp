#NAME $RENAME
#QUICK_DESCRIPTION rename the free variable.

#CALLING $COMMAND(<ident1>,<ident2>)
#TYPE $VOID_TYPE

#LIBRARYNAME void sollya_lib_name_free_variable(const char *)

#PARAMETERS <ident1> is the current name of the free variable.
#PARAMETERS <ident2> is a fresh name.


#DESCRIPTION
$COMMAND permits a change of the name of the free variable. $SOLLYA can handle
only one free variable at a time. The first time in a session that an
unbound name is used in a context where it can be interpreted as a free
variable, the name is used to represent the free variable of $SOLLYA. In the
following, this name can be changed using $COMMAND.

#DESCRIPTION
Be careful: if <ident2> has been set before, its value will be lost. Use
the command $ISBOUND to know if <ident2> is already used or not.

#DESCRIPTION
If <ident1> is not the current name of the free variable, an error occurs.

#DESCRIPTION
If $COMMAND is used at a time when the name of the free variable has not been
defined, <ident1> is just ignored and the name of the free variable is set
to <ident2>.

#DESCRIPTION
It is always possible to use the special keyword §§_x_§\verb|_x_|§§ to denote the free
variable. Hence <ident1> can be §§_x_§\verb|_x_|§§.

#DESCRIPTION
Notice that <ident2> is an identifier, not a string. In particular, it is
not directly possible to use the content of a variable to provide the new
name of the free variable. This can however be obtained using a level of
indirection: first create a string that contains the call to $COMMAND, and
then evaluate it thanks to $PARSE (see example below).

#EXAMPLE
f=sin(x);
f;
rename(x,y);
f;

#EXAMPLE
a=1;
f=sin(x);
rename(x,a);
a;
f;

#EXAMPLE
verbosity=1!;
f=sin(x);
rename(y, z);
rename(_x_, z);

#EXAMPLE
verbosity=1!;
rename(x,y);
isbound(x);
isbound(y);

#EXAMPLE
verbosity=1!;
f = sin(x);
new_varname = "foo";
rename(_x_, new_varname);
f;
rename(_x_, x);
new_varname = "foo";
str = "rename(_x_, " @ new_varname @ ");";
(parse("proc () { " @ str @ "}"))();
f;

#EXAMPLE
verbosity=1!;
f = sin(x);
f;
rename(x, _x_);
f;
g = cos(y);
f;
g;
rename(_x_, _x_);
f;
g;

#SEEALSO $ISBOUND
#SEEALSO $PARSE
