\subsection{bezout}
\label{labbezout}
\noindent Name: \textbf{bezout}\\
\phantom{aaa}Computes the gcd as well as the co-factors (Bezout coefficients)\\[0.2cm]
\noindent Library name:\\
\verb|   sollya_obj_t sollya_lib_bezout(sollya_obj_t, sollya_obj_t)|\\[0.2cm]
\noindent Usage: 
\begin{center}
\textbf{bezout}(\emph{p}, \emph{q}) : (\textsf{function}, \textsf{function}) $\rightarrow$ \textsf{structure}\\
\end{center}
Parameters: 
\begin{itemize}
\item \emph{p} is a constant or a polynomial.
\item \emph{q} is a constant or a polynomial.
\end{itemize}
\noindent Description: \begin{itemize}

\item When both \emph{p} and \emph{q} are integers, \textbf{bezout}(\emph{p},\emph{q}) computes the greatest
   common divisor $g$ of these two integers, \emph{i.e.}, the greatest non-negative
   integer $g$ dividing both \emph{p} and \emph{q}, as well as co-factors (Bezout
   coefficients), \emph{i.e.}, $a$ and $b$ such that $a \, p + b \, q = g$.

\item \textbf{bezout} always returns a structure containing four elements, \texttt{g}, \texttt{r}, \texttt{a} and \texttt{b}.
   These four elements always satisfy the following two properties:
   $\gcd(g,r) = \gcd(p,q)$ and $a \, p + b \, q = g$. In addition, the tool tries to
   ensure $r = 0$. This latter property cannot always be ensured, in particular,
   it cannot be ensured for constant expression such that $r$ becomes zero but
   the tool is unable to prove this fact. \textbf{bezout} ensures that $g$ is the greatest
   common divisor of \emph{p} and \emph{q} only if $r$ ends up being zero.

\item When both \emph{p} and \emph{q} are rational numbers, say $s/t$ and $u/v$,
   \textbf{bezout}(\emph{p},\emph{q}) computes the greatest common divisor of $s \cdot v$ and $u \cdot t$,
   divided by the product of the denominators, $t \cdot v$, as well as co-factors,
   still satisfying $a \, p + b \, q = g$.

\item When both \emph{p} and \emph{q} are constants but at least one of them is no rational
   number, \textbf{bezout} does not run the extended Euclidian algorithm to reduce
   $\gcd(p,q)$, \emph{i.e.}, the two elements \texttt{g}, \texttt{r} of the returned structure contain
   copies of the inputs \emph{p} and \emph{q}. The elements \texttt{a} and \texttt{b} are set to $0$ and $1$
   accordingly. \textbf{bezout} differs from the \textbf{gcd} command in this point.

\item When both \emph{p} and \emph{q} are polynomials with at least one being non-constant,
   \textbf{bezout}(\emph{p},\emph{q}) returns the polynomial of greatest degree dividing both \emph{p}
   and \emph{q}, and whose leading coefficient is the greatest common divisor of the
   leading coefficients of \emph{p} and \emph{q}. Polynomial co-factors (Bezout
   coefficients), \emph{i.e.}, $a$ and $b$ such that $a \, p + b \, q = g$, are returned as
   well.

\item Similarly to the cases documented for \textbf{div} and \textbf{mod}, \textbf{bezout} may fail to
   return the unique polynomial of largest degree dividing both \emph{p} and \emph{q} in
   cases when certain coefficients of either \emph{p} or \emph{q} are constant expressions
   for which the tool is unable to determine whether they are zero or not. These
   cases typically involve polynomials whose leading coefficient is zero but the
   tool is unable to detect this fact. In contrast to the \textbf{gcd} command, the
   \textbf{bezout} command is completely safe in this respect, as it returns not only
   an alleged greatest common divisor \texttt{g} but also the final remainder \texttt{r} obtained
   in the extended Euclidian algorithm. The two properties $\gcd(g,r) = \gcd(p,q)$
   and $a \, p + b \, q = g$ are always satisfied for the four elements of the
   returned structure.

\item When at least one of \emph{p} or \emph{q} is a function that is no polynomial, \textbf{bezout}
   does not run the extended Euclidian algorithm to reduce $\gcd(p,q)$, \emph{i.e.}, the
   two elements \texttt{g}, \texttt{r} of the returned structure contain copies of the inputs \emph{p}
   and \emph{q}. The elements \texttt{a} and \texttt{b} are set to $0$ and $1$ accordingly. \textbf{bezout} differs
   from the \textbf{gcd} command in this point.
\end{itemize}
\noindent Example 1: 
\begin{center}\begin{minipage}{15cm}\begin{Verbatim}[frame=single]
> bezout(1001, 231);
{ .b = -4, .a = 1, .r = 0, .g = 77 }
> bezout(13, 17);
{ .b = -3, .a = 4, .r = 0, .g = 1 }
> bezout(-210, 462);
{ .b = 1, .a = 2, .r = 0, .g = 42 }
\end{Verbatim}
\end{minipage}\end{center}
\noindent Example 2: 
\begin{center}\begin{minipage}{15cm}\begin{Verbatim}[frame=single]
> rationalmode = on!;
> bezout(6/7, 33/13);
{ .b = -1, .a = 3, .r = 0, .g = 3 / 91 }
\end{Verbatim}
\end{minipage}\end{center}
\noindent Example 3: 
\begin{center}\begin{minipage}{15cm}\begin{Verbatim}[frame=single]
> bezout(exp(13),sin(17));
{ .b = 0, .a = 1, .r = -0.96139749187955685726163694486915609849206725405894, .g
 = 4.4241339200892050332610277594908828178439130606059e5 }
\end{Verbatim}
\end{minipage}\end{center}
\noindent Example 4: 
\begin{center}\begin{minipage}{15cm}\begin{Verbatim}[frame=single]
> bezout(24 + 68 * x + 74 * x^2 + 39 * x^3 + 10 * x^4 + x^5, 480 + 776 * x + 476
 * x^2 + 138 * x^3 + 19 * x^4 + x^5);
{ .b = 9.1666666666666666666666666666666666666666666666665e-2 + x * (0.125 + x *
 5e-2), .a = -1.6666666666666666666666666666666666666666666666667 + x * (-0.575 
+ x * (-5e-2)), .r = 0, .g = 4 + x * (4 + x) }
> bezout(1001 * x^2, 231 * x);
{ .b = 0.33333333333333333333333333333333333333333333333333, .a = 0, .r = 0, .g 
= x * 77 }
\end{Verbatim}
\end{minipage}\end{center}
\noindent Example 5: 
\begin{center}\begin{minipage}{15cm}\begin{Verbatim}[frame=single]
> bezout(exp(x), x^2);
{ .b = 0, .a = 1, .r = x^2, .g = exp(x) }
\end{Verbatim}
\end{minipage}\end{center}
See also: \textbf{gcd} (\ref{labgcd}), \textbf{div} (\ref{labdiveucl}), \textbf{mod} (\ref{labmodeucl}), \textbf{numberroots} (\ref{labnumberroots})
